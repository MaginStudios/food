import 'package:food/src/models/food_detail_model.dart';
import 'package:food/src/models/food_search_model.dart';
import 'package:food/src/models/state.dart';
import 'package:food/src/utils/time_utils.dart';
import 'package:inject/inject.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class FoodCacheProvider {
  @provide
  FoodCacheProvider(this._sharedPreferences, this._minutesInCache);

  static const String CACHED_FOOD_SEARCH = 'cachedFoodSearch';
  static const String CACHED_FOOD_DETAIL = 'cachedFoodDetail';
  static const String CACHED_FOOD_SEARCH_TIME = 'cachedFoodSearchTime';
  static const String CACHED_FOOD_DETAIL_TIME = 'cachedFoodDetailTime';

  final Future<SharedPreferences> _sharedPreferences;
  final int _minutesInCache;

  void cacheFoodDetail(List<int> ids, FoodDetailModel foodDetailModel) async {
    final idsKey = ids.join(',');
    final prefs = await _sharedPreferences;
    prefs.setString(
        '$CACHED_FOOD_DETAIL$idsKey', json.encode(foodDetailModel.toJson()));
    prefs.setInt('$CACHED_FOOD_DETAIL_TIME$idsKey',
        DateTime.now().millisecondsSinceEpoch);
  }

  void cacheFoodSearch(String keyword, FoodSearchModel foodSearchModel) async {
    final prefs = await _sharedPreferences;
    prefs.setString(
        '$CACHED_FOOD_SEARCH$keyword', json.encode(foodSearchModel.toJson()));
    prefs.setInt('$CACHED_FOOD_SEARCH_TIME$keyword',
        DateTime.now().millisecondsSinceEpoch);
  }

  Future<State> fetchFoodDetail(List<int> ids) async {
    final idsKey = ids.join(',');
    final prefs = await _sharedPreferences;
    final jsonString = prefs.getString('$CACHED_FOOD_DETAIL$idsKey');
    int time = prefs.getInt('$CACHED_FOOD_DETAIL_TIME$idsKey');
    if (jsonString != null) {
      if (TimeUtils.differenceInMinutes(
              time, DateTime.now().millisecondsSinceEpoch) <
          _minutesInCache) {
        return State<FoodDetailModel>.success(
            FoodDetailModel.fromJson(json.decode(jsonString)));
      }
      prefs.remove('$CACHED_FOOD_DETAIL$idsKey');
      prefs.remove('$CACHED_FOOD_DETAIL_TIME$idsKey');
    }
    return State<String>.error("Food detail not cached");
  }

  Future<State> fetchFoodSearch(String keyword) async {
    final prefs = await _sharedPreferences;
    final jsonString = prefs.getString('$CACHED_FOOD_SEARCH$keyword');
    final time = prefs.getInt('$CACHED_FOOD_SEARCH_TIME$keyword');
    if (jsonString != null) {
      if (TimeUtils.differenceInMinutes(
              time, DateTime.now().millisecondsSinceEpoch) <
          _minutesInCache) {
        return State<FoodSearchModel>.success(
            FoodSearchModel.fromJson(json.decode(jsonString)));
      }
      prefs.remove('$CACHED_FOOD_SEARCH$keyword');
      prefs.remove('$CACHED_FOOD_SEARCH_TIME$keyword');
    }
    return State<String>.error("Food search not cached");
  }
}
