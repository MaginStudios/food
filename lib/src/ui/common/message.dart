import 'package:flutter/material.dart';

class Message extends StatelessWidget {
  Message(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, color: Colors.grey),
      ),
    );
  }
}
