import 'package:flutter/material.dart';

enum ThemeKey { LIGHT }

class Themes {
  static final lightTheme = ThemeData(
    primaryColor: Colors.deepOrange,
    accentColor: Colors.deepOrange,
    primaryColorDark: Colors.red,
    scaffoldBackgroundColor: const Color(0xFFECEFF1),
    unselectedWidgetColor: const Color(0xFFDDDDDD),
    hintColor: const Color(0xFFAAAAAA),
  );

  static ThemeData getFromKey(ThemeKey key) {
    switch (key) {
      case ThemeKey.LIGHT:
        return lightTheme;
      default:
        return lightTheme;
    }
  }
}
