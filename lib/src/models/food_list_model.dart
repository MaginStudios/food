class FoodListModel {
  FoodListModel(
    this.id,
    this.name,
    this.timestamp,
    this.category, [
    this.image,
    this.calories,
  ]);

  int id;
  String name;
  String image;
  double calories;
  int timestamp;
  int category;
}
