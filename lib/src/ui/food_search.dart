import 'package:flutter/material.dart';
import '../models/food_search_model.dart';
import '../blocs/food_search_bloc.dart';
import 'common/loading.dart';
import 'common/message.dart';

class FoodSearch extends StatefulWidget {
  FoodSearch(this.bloc) : super();

  final FoodSearchBloc bloc;

  @override
  State<StatefulWidget> createState() => _FoodSearchState();
}

class _FoodSearchState extends State<FoodSearch> {
  var _isInitialState = true;
  Icon _searchIcon;
  Widget _appBarTitle;
  Color _appBarBackgroundColor;
  Color _appBarIconsColor;
  TextEditingController _searchController;

  @override
  void initState() {
    super.initState();
    _searchIcon = Icon(Icons.search);
    _appBarTitle = Text('Search Food');
    _appBarIconsColor = Colors.white;
    WidgetsBinding.instance.addPostFrameCallback((_) => _handleSearchAppBar());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    widget.bloc.init();
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  void _handleSearchAppBar() {
    setState(() {
      if (_searchIcon.icon == Icons.search) {
        _searchIcon = Icon(Icons.close, color: Colors.grey);
        _appBarTitle = TextField(
          onSubmitted: (text) {
            setState(() {
              _isInitialState = false;
              widget.bloc.fetchFoodSearch(text);
            });
          },
          textCapitalization: TextCapitalization.sentences,
          autofocus: true,
          style: TextStyle(fontSize: 20),
          controller: _searchController,
          decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent)),
              hintText: 'Search...'),
        );
        _appBarBackgroundColor = Colors.white;
        _appBarIconsColor = Colors.grey;
      } else {
        _searchIcon = Icon(Icons.search);
        _appBarTitle = Text('Search Food');
        _appBarBackgroundColor = Theme.of(context).primaryColor;
        _appBarIconsColor = Colors.white;
      }
    });
  }

  Widget _buildFoodSearch(AsyncSnapshot<FoodSearchModel> snapshot) {
    return GridView.builder(
      padding: EdgeInsets.all(4),
      itemCount: snapshot.data.results.length,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (context, index) {
        return GridTile(
          child: InkResponse(
            enableFeedback: true,
            child: Padding(
              padding: EdgeInsets.all(4),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                        ),
                        image: DecorationImage(
                          image: NetworkImage(
                            '${snapshot.data.baseUri}${snapshot.data.results[index].image}',
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(12),
                        child: Text(
                          '${snapshot.data.results[index].title}',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            onTap: () => _goToFoodDetail(
              snapshot.data.results[index].id,
              snapshot.data.results[index].title,
              '${snapshot.data.baseUri}${snapshot.data.results[index].image}',
            ),
          ),
        );
      },
    );
  }

  void _goToFoodDetail(int id, String name, String image) {
    Navigator.pushNamed(context, 'foodDetail', arguments: {
      'name': name,
      'id': id,
      'image': image,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        iconTheme: IconThemeData(color: _appBarIconsColor),
        backgroundColor: _appBarBackgroundColor,
        title: _appBarTitle,
        actions: <Widget>[
          IconButton(icon: _searchIcon, onPressed: () => _handleSearchAppBar()),
        ],
      ),
      body: StreamBuilder<FoodSearchModel>(
        stream: widget.bloc.foodSearch,
        builder: (context, snapshot) {
          if (snapshot.hasData || _isInitialState) {
            if (_isInitialState) {
              return SizedBox.shrink();
            } else if (snapshot.data.results.isNotEmpty) {
              return _buildFoodSearch(snapshot);
            }
            return Message('No food found');
          } else if (snapshot.hasError) {
            return Message(snapshot.error);
          }
          return Loading();
        },
      ),
    );
  }
}
