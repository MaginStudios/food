class FoodDetailModel {
  FoodDetailModel.fromJson(dynamic parsedJson) {
    for (int i = 0; i < parsedJson.length; i++) {
      _Result result = _Result(parsedJson[i]);
      _results.add(result);
    }
  }

  List<_Result> _results = [];

  List<_Result> get results => _results;

  dynamic toJson() => _results.map((result) => result.toJson()).toList();
}

class _Result {
  _Result(result) {
    _id = result['id'];
    _image = result['image'];
    _title = result['title'];
    _nutrition = _Nutrition.fromJson(result['nutrition']);
  }

  int _id;
  String _title;
  String _image;
  _Nutrition _nutrition;

  int get id => _id;

  String get title => _title;

  String get image => _image;

  _Nutrition get nutrition => _nutrition;

  double get calories => _nutrition.nutrients.first.amount;

  dynamic toJson() => {
        'id': _id,
        'image': _image,
        'title': _title,
        'nutrition': _nutrition.toJson(),
      };
}

class _Nutrition {
  _Nutrition.fromJson(dynamic nutrition) {
    nutrition['nutrients']
        .forEach((nutrient) => _nutrients.add(_Nutrient.fromJson(nutrient)));
    _caloricBreakdown =
        _CaloricBreakdown.fromJson(nutrition['caloricBreakdown']);
  }

  List<_Nutrient> _nutrients = [];
  _CaloricBreakdown _caloricBreakdown;

  List<_Nutrient> get nutrients => _nutrients;

  _CaloricBreakdown get caloricBreakdown => _caloricBreakdown;

  dynamic toJson() => {
        'nutrients': _nutrients.map((nutrient) => nutrient.toJson()).toList(),
        'caloricBreakdown': _caloricBreakdown.toJson(),
      };
}

class _Nutrient {
  _Nutrient.fromJson(nutrient) {
    _title = nutrient['title'];
    _amount = nutrient['amount'];
    _unit = nutrient['unit'];
  }

  String _title;
  double _amount;
  String _unit;

  String get title => _title;

  double get amount => _amount;

  String get unit => _unit;

  dynamic toJson() => {
        'title': _title,
        'amount': _amount,
        'unit': _unit,
      };
}

class _CaloricBreakdown {
  _CaloricBreakdown.fromJson(caloricBreakdown) {
    _percentProtein = caloricBreakdown['percentProtein'];
    _percentFat = caloricBreakdown['percentFat'];
    _percentCarbs = caloricBreakdown['percentCarbs'];
  }

  double _percentProtein;
  double _percentFat;
  double _percentCarbs;

  double get percentProtein => _percentProtein;

  double get percentFat => _percentFat;

  double get percentCarbs => _percentCarbs;

  dynamic toJson() => {
        'percentProtein': _percentProtein,
        'percentFat': _percentFat,
        'perentCarbs': _percentCarbs,
      };
}
