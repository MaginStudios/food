import 'package:food/src/resources/food_api_provider.dart';
import 'package:food/src/resources/food_cache_provider.dart';
import 'package:food/src/resources/food_database_provider.dart';
import 'package:inject/inject.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart' as pref;

import 'mock_client.dart';

@module
class ProviderModule {
  @provide
  @singleton
  http.Client client() => MockClient();

  @provide
  @singleton
  int minutesInCache() => 60;

  @provide
  @singleton
  Future<pref.SharedPreferences> sharedPreferences() async {
    pref.SharedPreferences.setMockInitialValues({});
    return await pref.SharedPreferences.getInstance();
  }

  @provide
  @singleton
  FoodApiProvider foodApiProvider(http.Client client) =>
      FoodApiProvider(client);

  @provide
  @singleton
  FoodCacheProvider foodCacheProvider(
          Future<pref.SharedPreferences> sharedPreferences,
          int minutesInCache) =>
      FoodCacheProvider(sharedPreferences, minutesInCache);

  @provide
  @singleton
  FoodDatabaseProvider foodDatabaseProvider() => FoodDatabaseProvider.instance;
}
