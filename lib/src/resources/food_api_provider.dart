import 'package:food/src/models/food_detail_model.dart';
import 'package:food/src/models/food_search_model.dart';
import 'package:food/src/models/state.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart';
import 'dart:convert';
import 'package:inject/inject.dart';

class FoodApiProvider {
  @provide
  FoodApiProvider(this._client);

  final Client _client;
  final _baseUrl = 'https://api.spoonacular.com/recipes';
  final _apiKey = '88b61bd4b73c427ca6901e444f0701ca';

  Future<State> fetchFoodSearch(String keyword) async {
    try {
      final response = await _client
          .get('$_baseUrl/search?query=$keyword&number=6&apiKey=$_apiKey');
      if (response != null && response.statusCode == 200) {
        return State<FoodSearchModel>.success(
            FoodSearchModel.fromJson(json.decode(response.body)));
      }
    } catch (_) {
      // TODO: Handle exception
    }
    return State<String>.error('Failed to load food search');
  }

  Future<State> fetchFoodDetail(List<int> ids) async {
    try {
      final response = await _client.get(
          '$_baseUrl/informationBulk?ids=${ids.join(',')}&includeNutrition=true&apiKey=$_apiKey');
      if (response != null && response.statusCode == 200) {
        return State<FoodDetailModel>.success(
            FoodDetailModel.fromJson(json.decode(response.body)));
      }
    } catch (_) {
      // TODO: Handle exception
    }
    return State<String>.error('Failed to load food detail');
  }
}
