import 'package:food/src/resources/model_mapper.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart'
    show getApplicationDocumentsDirectory;
import 'food_database_provider.dart';

abstract class DatabaseProvider<T> {
  String table();

  String columnId();

  String databaseName();

  int databaseVersion();

  FoodDatabaseProvider specificInstance();

  String createTableQuery();

  Database specificDatabase();

  void setSpecificDatabase(Database database);

  ModelMapper<T> modelMapper();

  Future<Database> get database async {
    if (specificDatabase() != null) return specificDatabase();
    setSpecificDatabase(await _initDatabase());
    return specificDatabase();
  }

  Future<Database> _initDatabase() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, databaseName());
    return await openDatabase(path,
        version: databaseVersion(), onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute(createTableQuery());
  }

  Future<int> insert(T model) async {
    final db = await specificInstance().database;
    return await db.insert(table(), modelMapper().toEntity(model),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<T>> queryAllRows(String orderByColumn, bool isDescending) async {
    final db = await specificInstance().database;
    final orderByType = isDescending ? 'DESC' : 'ASC';
    final list =
        await db.query(table(), orderBy: '$orderByColumn $orderByType');
    return list.map((item) => modelMapper().toModel(item)).toList();
  }

  Future<int> queryRowCount() async {
    final db = await specificInstance().database;
    return Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM ${table()}'));
  }

  Future<int> update(T model) async {
    final row = modelMapper().toEntity(model);
    final db = await specificInstance().database;
    final id = row[columnId()];
    return await db
        .update(table(), row, where: '${columnId()} = ?', whereArgs: [id]);
  }

  Future<int> delete(int id) async {
    final db = await specificInstance().database;
    return await db
        .delete(table(), where: '${columnId()} = ?', whereArgs: [id]);
  }
}
