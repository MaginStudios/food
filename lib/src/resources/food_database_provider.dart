import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/resources/food_list_model_mapper.dart';
import 'package:food/src/resources/database_provider.dart';
import 'package:food/src/resources/model_mapper.dart';
import 'package:inject/inject.dart';
import 'package:sqflite/sqflite.dart';

class FoodDatabaseProvider extends DatabaseProvider<FoodListModel> {
  FoodDatabaseProvider._();

  @provide
  static final FoodDatabaseProvider instance = FoodDatabaseProvider._();
  static Database _database;

  @override
  String createTableQuery() => '''
          CREATE TABLE ${table()} (
            ${FoodListModelMapper.id} INTEGER PRIMARY KEY,
            ${FoodListModelMapper.name} TEXT NOT NULL,
            ${FoodListModelMapper.timestamp} INTEGER NOT NULL,
            ${FoodListModelMapper.category} INTEGER NOT NULL
          )
          ''';

  @override
  FoodDatabaseProvider specificInstance() => instance;

  @override
  void setSpecificDatabase(Database database) => _database = database;

  @override
  Database specificDatabase() => _database;

  @override
  String columnId() => FoodListModelMapper.id;

  @override
  String databaseName() => 'food.db';

  @override
  int databaseVersion() => 1;

  @override
  String table() => 'food';

  @override
  ModelMapper<FoodListModel> modelMapper() => FoodListModelMapper();
}
