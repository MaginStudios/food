import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:food/src/blocs/food_list_bloc.dart';
import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/resources/food_list_model_mapper.dart';
import 'package:food/src/utils/time_utils.dart';
import 'common/loading.dart';
import 'common/message.dart';
import 'common/separator.dart';

class FoodList extends StatefulWidget {
  FoodList(this.bloc, [this.toastMsg]) : super();

  final toastMsg;
  final FoodListBloc bloc;

  @override
  State<StatefulWidget> createState() => _FoodListState();
}

class _FoodListState extends State<FoodList> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _showToast());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    widget.bloc.init();
    widget.bloc.fetchFoodList(FoodListModelMapper.timestamp, true);
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  void _showToast() {
    if (widget.toastMsg != null) {
      Fluttertoast.showToast(msg: widget.toastMsg);
    }
  }

  Widget _buildFoodList(AsyncSnapshot<List<FoodListModel>> snapshot) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          title: _buildTotalHeader(snapshot),
          pinned: true,
          elevation: 0,
          backgroundColor: Theme.of(context).primaryColorDark,
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              if (snapshot.data.isEmpty) {
                return Padding(
                  padding: const EdgeInsets.all(32),
                  child: Message('Tap the plus button to\nadd a new food'),
                );
              }
              return InkWell(
                enableFeedback: true,
                child: _buildItem(snapshot.data[index]),
                onTap: () => _goToFoodDetail(
                    snapshot.data[index].id,
                    snapshot.data[index].name,
                    snapshot.data[index].image,
                    snapshot.data[index].timestamp,
                    snapshot.data[index].calories,
                    snapshot.data[index].category),
              );
            },
            childCount: snapshot.data.isEmpty ? 1 : snapshot.data.length,
          ),
        ),
      ],
    );
  }

  Widget _buildTotalHeader(AsyncSnapshot<List<FoodListModel>> snapshot) {
    return Container(
      padding: EdgeInsets.only(top: 16, bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'Total',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            '${snapshot.data.fold(0.0, (total, item) => total += item.calories).toStringAsFixed(2)} cal',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(FoodListModel item) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).unselectedWidgetColor,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  image: DecorationImage(
                    image: NetworkImage('${item.image}'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Flexible(
                fit: FlexFit.tight,
                child: Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text(
                              item.name,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: 17),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4),
                      Text(
                        TimeUtils.formatDateTime(item.timestamp),
                        style: TextStyle(
                          fontSize: 12,
                          color: Theme.of(context).hintColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Text('${item.calories} cal', style: TextStyle(fontSize: 18)),
            ],
          ),
        ),
        Separator(),
      ],
    );
  }

  void _goToFoodDetail(int id, String name, String image, int timestamp,
      double calories, int category) {
    Navigator.pushNamed(context, 'foodDetail', arguments: {
      'id': id,
      'name': name,
      'image': image,
      'calories': calories,
      'timestamp': timestamp,
      'category': category,
      'isEditMode': true,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).cardColor,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text('Food')),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.pushNamed(context, 'foodSearch'),
      ),
      body: StreamBuilder<List<FoodListModel>>(
        stream: widget.bloc.foodList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return _buildFoodList(snapshot);
          } else if (snapshot.hasError) {
            return Message(snapshot.error);
          }
          return Loading();
        },
      ),
    );
  }
}
