import 'package:flutter/material.dart';
import 'package:food/src/ui/common/themes.dart';
import 'package:food/src/ui/food_detail.dart';
import 'package:food/src/ui/food_list.dart';
import 'package:food/src/ui/food_search.dart';
import 'package:inject/inject.dart';
import 'blocs/food_detail_bloc.dart';
import 'blocs/food_list_bloc.dart';
import 'blocs/food_search_bloc.dart';

class App extends StatelessWidget {
  @provide
  App(this.foodListBloc, this.foodSearchBloc, this.foodDetailBloc) : super();

  final FoodListBloc foodListBloc;
  final FoodSearchBloc foodSearchBloc;
  final FoodDetailBloc foodDetailBloc;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Themes.getFromKey(ThemeKey.LIGHT),
      onGenerateRoute: (settings) {
        Map<String, dynamic> args = settings.arguments;
        if (settings.name == 'foodDetail') {
          return MaterialPageRoute(builder: (context) {
            return FoodDetail(
              foodDetailBloc,
              args['id'],
              args['name'],
              args['image'],
              args['timestamp'],
              args['calories'],
              args['category'],
              args['isEditMode'],
            );
          });
        } else if (settings.name == 'foodSearch') {
          return MaterialPageRoute(builder: (context) {
            return FoodSearch(
              foodSearchBloc,
            );
          });
        } else if (settings.name == 'foodList') {
          return MaterialPageRoute(builder: (context) {
            return FoodList(
              foodListBloc,
              args['toastMsg'],
            );
          });
        }
        return null;
      },
      routes: {'/': (context) => FoodList(foodListBloc)},
    );
  }
}
