class FoodSearchModel {
  FoodSearchModel.fromJson(dynamic parsedJson) {
    parsedJson['results'].forEach((result) => _results.add(_Result(result)));
    _baseUri = parsedJson['baseUri'];
  }

  String _baseUri;
  List<_Result> _results = [];

  List<_Result> get results => _results;

  String get baseUri => _baseUri;

  dynamic toJson() => {
        'baseUri': _baseUri,
        'results': _results.map((result) => result.toJson()).toList(),
      };
}

class _Result {
  _Result(result) {
    _id = result['id'];
    _title = result['title'];
    _image = result['image'];
  }

  int _id;
  String _title;
  String _image;

  int get id => _id;

  String get title => _title;

  String get image => _image;

  dynamic toJson() => {
        'id': _id,
        'title': _title,
        'image': _image,
      };
}
