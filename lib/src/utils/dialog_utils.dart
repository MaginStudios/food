import 'package:flutter/material.dart';

class DialogUtils {
  static showConfirmationDialog(BuildContext context, String title,
      String message, VoidCallback onAccept) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          titlePadding: EdgeInsets.only(left: 22, top: 18),
          contentPadding: EdgeInsets.only(left: 22, top: 8, right: 22),
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('CANCEL'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                onAccept();
              },
            ),
          ],
        );
      },
    );
  }
}
