import 'package:food/src/blocs/bloc_base.dart';
import 'package:food/src/models/food_detail_model.dart';
import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/models/state.dart';
import 'package:food/src/resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:inject/inject.dart';

class FoodListBloc extends BlocBase {
  @provide
  FoodListBloc(this._repository);

  final Repository _repository;
  PublishSubject<List<FoodListModel>> _list;

  Observable<List<FoodListModel>> get foodList => _list.stream;

  void init() {
    _list = PublishSubject<List<FoodListModel>>();
  }

  void fetchFoodList(String orderByColumn, bool isDescending) async {
    final listModel =
        await _repository.fetchFoodList(orderByColumn, isDescending);
    if (listModel.isNotEmpty) {
      final state = await _repository
          .fetchFoodDetailByIds(listModel.map((food) => food.id).toList());
      if (state is SuccessState) {
        FoodDetailModel detailModel = state.value;
        for (final food in listModel) {
          final foodDetail =
              detailModel.results.firstWhere((detail) => detail.id == food.id);
          food.calories = foodDetail.calories;
          food.image = foodDetail.image;
        }
        _list.sink.add(listModel);
      } else {
        _list.sink.addError("Failed to get the list");
      }
    } else {
      _list.sink.add(listModel);
    }
  }

  void dispose() async {
    await _list.drain();
    _list.close();
  }
}
