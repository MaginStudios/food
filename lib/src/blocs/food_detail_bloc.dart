import 'package:food/src/blocs/bloc_base.dart';
import 'package:food/src/models/food_detail_model.dart';
import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/models/state.dart';
import 'package:food/src/resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:inject/inject.dart';

class FoodDetailBloc extends BlocBase {
  @provide
  FoodDetailBloc(this._repository);

  final Repository _repository;
  PublishSubject<FoodDetailModel> _detail;
  PublishSubject<int> _insertedId;
  PublishSubject<int> _updatedCount;
  PublishSubject<int> _deletedCount;

  Observable<FoodDetailModel> get foodDetail => _detail.stream;

  Observable<int> get insertedFoodId => _insertedId.stream;

  Observable<int> get deletedFoodCount => _deletedCount.stream;

  void init() {
    _detail = PublishSubject<FoodDetailModel>();
    _insertedId = PublishSubject<int>();
    _updatedCount = PublishSubject<int>();
    _deletedCount = PublishSubject<int>();
  }

  void deleteFood(int id) async {
    final deletedCount = await _repository.deleteFood(id);
    _deletedCount.sink.add(deletedCount);
  }

  void insertFood(FoodListModel food) async {
    final id = await _repository.insertFood(food);
    _insertedId.sink.add(id);
  }

  void updateFood(FoodListModel food) async {
    final updatedCount = await _repository.updateFood(food);
    _updatedCount.sink.add(updatedCount);
  }

  void fetchFoodDetailById(int id) async {
    final state = await _repository.fetchFoodDetailById(id);
    if (state is SuccessState) {
      _detail.sink.add(state.value);
    } else {
      _detail.addError((state as ErrorState).msg);
    }
  }

  void dispose() async {
    await _detail.drain();
    _detail.close();
    _insertedId.close();
    _deletedCount.close();
    _updatedCount.close();
  }
}
