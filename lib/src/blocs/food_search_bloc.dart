import 'package:food/src/blocs/bloc_base.dart';
import 'package:food/src/models/food_search_model.dart';
import 'package:food/src/models/state.dart';
import 'package:food/src/resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:inject/inject.dart';

class FoodSearchBloc extends BlocBase {
  @provide
  FoodSearchBloc(this._repository);

  Repository _repository;
  PublishSubject<FoodSearchModel> _search;

  Observable<FoodSearchModel> get foodSearch => _search.stream;

  void init() {
    _search = PublishSubject<FoodSearchModel>();
  }

  void fetchFoodSearch(String keyword) async {
    final state = await _repository.fetchFoodSearch(keyword);
    if (state is SuccessState) {
      _search.sink.add(state.value);
    } else {
      _search.addError((state as ErrorState).msg);
    }
  }

  void dispose() async {
    await _search.drain();
    _search.close();
  }
}
