abstract class ModelMapper<T> {
  Map<String, dynamic> toEntity(T model);

  T toModel(Map<String, dynamic> entity);
}
