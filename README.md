<p align="center">
 <img src ="https://gitlab.com/MaginStudios/food/raw/master/food_app_1.png", height=350/>
 <img src ="https://gitlab.com/MaginStudios/food/raw/master/food_app_2.png", height=350/>
 <img src ="https://gitlab.com/MaginStudios/food/raw/master/food_app_3.png", height=350/>
</p>

# Food · Flutter App
This is a food tracking application. It includes the following operations:
- Add a new entry
- Edit an entry
- Delete an entry
- Get a list of entries

Technical details:
- **Dart** · Language
- **BLoC** · Architecture
- **Streams, RxDart** · Reactive Programming
- **Inject** · Dependency Injection
- **Http** · Network Requests
- **Sqflite** · Persistence
- **Inject, Mockito** · Unit Tests & Integration Tests