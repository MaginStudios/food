import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TimeUtils {
  static formatDateTime(int timestamp) {
    final date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    var format = 'HH:mm · dd/MM/yy';
    var tempDate = DateTime.now();
    if (isSameDate(date, tempDate)) {
      format = 'HH:mm · \'Today\'';
    } else {
      tempDate = DateTime.now().subtract(Duration(days: 1));
      if (isSameDate(date, tempDate)) {
        format = 'HH:mm · \'Yesterday\'';
      }
    }
    return DateFormat(format).format(date);
  }

  static formatDate(int timestamp) {
    final date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    var tempDate = DateTime.now();
    if (isSameDate(date, tempDate)) {
      return 'Today';
    } else {
      tempDate = DateTime.now().subtract(Duration(days: 1));
      if (isSameDate(date, tempDate)) {
        return 'Yesterday';
      }
    }
    return DateFormat('dd/MM/yy').format(date);
  }

  static formatTime(int hour, int minute) =>
      '${hour.toString().padLeft(2, '0')}:${minute.toString().padLeft(2, '0')}';

  static isSameDate(DateTime date1, DateTime date2) =>
      date1.day == date2.day &&
      date1.month == date2.month &&
      date1.year == date2.year;

  static DateTime buildDateTimeWith(DateTime date, TimeOfDay timeOfDay) =>
      DateTime(
          date.year, date.month, date.day, timeOfDay.hour, timeOfDay.minute);

  static int differenceInMinutes(int timestamp1, int timestamp2) =>
      (timestamp2 - timestamp1).abs() ~/ 1000 ~/ 60;
}
