import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/resources/model_mapper.dart';

class FoodListModelMapper implements ModelMapper<FoodListModel> {
  static const String id = 'id';
  static const String name = 'name';
  static const String timestamp = 'timestamp';
  static const String category = 'category';

  Map<String, dynamic> toEntity(FoodListModel foodListModel) => {
        id: foodListModel.id,
        name: foodListModel.name,
        timestamp: foodListModel.timestamp,
        category: foodListModel.category,
      };

  FoodListModel toModel(Map<String, dynamic> foodListEntity) => FoodListModel(
        foodListEntity[id],
        foodListEntity[name],
        foodListEntity[timestamp],
        foodListEntity[category],
      );
}
