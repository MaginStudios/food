import 'package:food/src/resources/food_cache_provider.dart';
import 'package:food/src/resources/food_database_provider.dart';
import 'package:inject/inject.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../blocs/food_list_bloc.dart';
import '../blocs/food_search_bloc.dart';
import '../blocs/food_detail_bloc.dart';
import '../blocs/bloc_base.dart';
import '../resources/repository.dart';
import '../resources/food_api_provider.dart';
import 'package:http/http.dart' show Client;

@module
class BlocModule {
  @provide
  @singleton
  Client client() => Client();

  @provide
  @singleton
  Future<SharedPreferences> sharedPreferences() =>
      SharedPreferences.getInstance();

  @provide
  @singleton
  int minutesInCache() => 60;

  @provide
  @singleton
  FoodApiProvider foodApiProvider(Client client) => FoodApiProvider(client);

  @provide
  @singleton
  FoodDatabaseProvider foodDatabaseProvider() => FoodDatabaseProvider.instance;

  @provide
  @singleton
  FoodCacheProvider foodCacheProvider(
          Future<SharedPreferences> sharedPreferences, int minutesInCache) =>
      FoodCacheProvider(sharedPreferences, minutesInCache);

  @provide
  @singleton
  Repository repository(
          FoodApiProvider foodApiProvider,
          FoodDatabaseProvider foodDatabaseProvider,
          FoodCacheProvider foodCacheProvider) =>
      Repository(foodApiProvider, foodDatabaseProvider, foodCacheProvider);

  @provide
  BlocBase foodListBloc(Repository repository) => FoodListBloc(repository);

  @provide
  BlocBase foodSearchBloc(Repository repository) => FoodSearchBloc(repository);

  @provide
  BlocBase foodDetailBloc(Repository repository) => FoodDetailBloc(repository);
}
