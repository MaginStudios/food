import 'provider_injector.dart' as _i1;
import 'provider_module.dart' as _i2;
import 'package:http/src/client.dart' as _i3;
import 'package:food/src/resources/food_api_provider.dart' as _i4;
import 'package:food/src/resources/food_database_provider.dart' as _i5;
import 'dart:async' as _i6;
import 'package:food/src/resources/food_cache_provider.dart' as _i7;
import 'provider_test.dart' as _i8;

class ProviderTestInjector$Injector implements _i1.ProviderTestInjector {
  ProviderTestInjector$Injector._(this._providerModule);

  final _i2.ProviderModule _providerModule;

  _i3.Client _singletonClient;

  _i4.FoodApiProvider _singletonFoodApiProvider;

  _i5.FoodDatabaseProvider _singletonFoodDatabaseProvider;

  _i6.Future _singletonFuture;

  int _singletonInt;

  _i7.FoodCacheProvider _singletonFoodCacheProvider;

  static _i6.Future<_i1.ProviderTestInjector> create(
      _i2.ProviderModule providerModule) async {
    final injector = ProviderTestInjector$Injector._(providerModule);

    return injector;
  }

  _i8.ProviderTest _createProviderTest() => _i8.ProviderTest(
      _createFoodApiProvider(),
      _createFoodDatabaseProvider(),
      _createFoodCacheProvider(),
      _createFuture(),
      _createClient(),
      _createInt());
  _i4.FoodApiProvider _createFoodApiProvider() => _singletonFoodApiProvider ??=
      _providerModule.foodApiProvider(_createClient());
  _i3.Client _createClient() => _singletonClient ??= _providerModule.client();
  _i5.FoodDatabaseProvider _createFoodDatabaseProvider() =>
      _singletonFoodDatabaseProvider ??= _providerModule.foodDatabaseProvider();
  _i7.FoodCacheProvider _createFoodCacheProvider() =>
      _singletonFoodCacheProvider ??=
          _providerModule.foodCacheProvider(_createFuture(), _createInt());
  _i6.Future _createFuture() =>
      _singletonFuture ??= _providerModule.sharedPreferences();
  int _createInt() => _singletonInt ??= _providerModule.minutesInCache();
  @override
  _i8.ProviderTest get app => _createProviderTest();
}
