import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:food/src/blocs/food_detail_bloc.dart';
import 'package:food/src/models/food_detail_model.dart';
import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/utils/dialog_utils.dart';
import 'package:food/src/utils/time_utils.dart';

import 'common/loading.dart';
import 'common/message.dart';
import 'common/separator.dart';

class FoodDetail extends StatefulWidget {
  FoodDetail(
    this.bloc,
    this.id,
    this.name,
    this.image,
    this.timestamp,
    this.calories,
    this.category,
    bool isEditMode,
  )   : this.isEditMode = isEditMode ?? false,
        super();

  final int id;
  final String name;
  final String image;
  final double calories;
  final int timestamp;
  final int category;
  final bool isEditMode;
  final FoodDetailBloc bloc;

  @override
  State<StatefulWidget> createState() => _FoodDetailState();
}

class _FoodDetailState extends State<FoodDetail> {
  var _detailsLoaded = false;
  TextEditingController _textEditingController;
  String _categorySelected;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  Map<String, int> _categoriesMap = {
    'Breakfast': 1,
    'Lunch': 2,
    'Dinner': 3,
    'Other': 4,
  };

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(text: widget.name);
    _selectedDate = widget.isEditMode
        ? DateTime.fromMillisecondsSinceEpoch(widget.timestamp)
        : DateTime.now();
    _selectedTime = widget.isEditMode
        ? TimeOfDay.fromDateTime(_selectedDate)
        : TimeOfDay.now();
    _categorySelected = widget.isEditMode
        ? _categoriesMap.keys
            .firstWhere((key) => _categoriesMap[key] == widget.category)
        : _categoriesMap.keys.first;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    widget.bloc.init();
    widget.bloc.fetchFoodDetailById(widget.id);
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime.fromMillisecondsSinceEpoch(0),
      lastDate: DateTime(2200),
    );
    if (picked != null && picked != _selectedDate) {
      setState(() => _selectedDate = picked);
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
    );
    if (picked != null && picked != _selectedTime) {
      setState(() => _selectedTime = picked);
    }
  }

  void _goToFoodList(String toastMsg) {
    Navigator.pushNamedAndRemoveUntil(
        context, 'foodList', ModalRoute.withName('foodList'),
        arguments: {
          'toastMsg': toastMsg,
        });
  }

  Widget _buildCardInfo(String title, String description) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Text(
              title.toUpperCase(),
              style:
                  TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
            ),
            SizedBox(height: 8),
            Text(
              description,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionInput() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 12),
            child: Icon(Icons.create, size: 18),
          ),
          Flexible(
            child: TextFormField(
              controller: _textEditingController,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 4, bottom: 4),
                hintText: 'Description',
              ),
              style: TextStyle(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCategoryInput() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 12),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 12),
            child: Icon(Icons.fastfood, size: 18),
          ),
          Flexible(
            fit: FlexFit.tight,
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                items: _categoriesMap.keys
                    .map((name) =>
                        DropdownMenuItem(value: name, child: Text(name)))
                    .toList(),
                onChanged: (name) => setState(() => _categorySelected = name),
                value: _categorySelected,
                isExpanded: true,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDateAndTimeInput() {
    return Row(
      children: <Widget>[
        Flexible(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              child: Padding(
                padding: EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 12),
                      child: Icon(Icons.access_time, size: 18),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        TimeUtils.formatTime(
                            _selectedTime.hour, _selectedTime.minute),
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: Icon(Icons.arrow_drop_down),
                    ),
                  ],
                ),
              ),
              onTap: () => _selectTime(context),
            ),
          ),
        ),
        Flexible(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              child: Padding(
                padding: EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 16, right: 12),
                      child: Icon(Icons.today, size: 18),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        TimeUtils.formatDate(
                            _selectedDate.millisecondsSinceEpoch),
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down),
                  ],
                ),
              ),
              onTap: () => _selectDate(context),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDetailInput() {
    return Container(
      color: Theme.of(context).cardColor,
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(bottom: 16),
      child: Column(
        children: <Widget>[
          _buildDescriptionInput(),
          _buildCategoryInput(),
          _buildDateAndTimeInput(),
          Separator(),
        ],
      ),
    );
  }

  Widget _buildFoodDetail(FoodDetailModel foodDetail) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _buildCardInfo(
              'Calories', '${foodDetail.results.first.calories} cal'),
          Row(
            children: <Widget>[
              Flexible(
                fit: FlexFit.tight,
                child: _buildCardInfo('Protein',
                    '${foodDetail.results.first.nutrition.caloricBreakdown.percentProtein}%'),
              ),
              Flexible(
                fit: FlexFit.tight,
                child: _buildCardInfo('Fat',
                    '${foodDetail.results.first.nutrition.caloricBreakdown.percentFat}%'),
              ),
              Flexible(
                fit: FlexFit.tight,
                child: _buildCardInfo('Carbs',
                    '${foodDetail.results.first.nutrition.caloricBreakdown.percentCarbs}%'),
              ),
            ],
          ),
          _buildCardInfo(
              'More Information',
              foodDetail.results.first.nutrition.nutrients.sublist(1).fold(
                  '',
                  (prev, nutrient) =>
                      '$prev${nutrient.title}: ${nutrient.amount} ${nutrient.unit}\n')),
        ],
      ),
    );
  }

  void _insertOrUpdateFood() {
    final editedFood = FoodListModel(
      widget.id,
      _textEditingController.text,
      TimeUtils.buildDateTimeWith(_selectedDate, _selectedTime)
          .millisecondsSinceEpoch,
      _categoriesMap[_categorySelected],
    );
    if (widget.isEditMode) {
      widget.bloc.updateFood(editedFood);
      // TODO: Check if the food was updated successfully
      _goToFoodList('Food updated successfully');
    } else if (_detailsLoaded) {
      widget.bloc.insertFood(editedFood);
      // TODO: Check if the food was inserted successfully
      _goToFoodList('Food added successfully');
    }
  }

  void _deleteFood() {
    if (_detailsLoaded) {
      DialogUtils.showConfirmationDialog(
          context, 'Delete Food', 'Are you sure you want to delete this food?',
          () {
        widget.bloc.deleteFood(widget.id);
        // TODO: Check if the food was deleted successfully
        _goToFoodList('Food deleted successfully');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        onPressed: () => _insertOrUpdateFood(),
      ),
      appBar: AppBar(
        title: Text(widget.isEditMode ? 'Edit Food' : 'New Food'),
        actions: <Widget>[
          Visibility(
            visible: widget.isEditMode,
            child: IconButton(
              icon: Icon(Icons.delete),
              onPressed: () => _deleteFood(),
            ),
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.only(bottom: 64),
        children: <Widget>[
          _buildDetailInput(),
          StreamBuilder<FoodDetailModel>(
            stream: widget.bloc.foodDetail,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                _detailsLoaded = true;
                if (snapshot.data.results.isNotEmpty) {
                  return _buildFoodDetail(snapshot.data);
                }
                return Message('No food details');
              } else if (snapshot.hasError) {
                return Message(snapshot.error);
              }
              return Loading();
            },
          ),
        ],
      ),
    );
  }
}
