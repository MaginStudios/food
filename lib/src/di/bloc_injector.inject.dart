import 'bloc_injector.dart' as _i1;
import 'bloc_module.dart' as _i2;
import 'package:http/src/client.dart' as _i3;
import '../resources/food_api_provider.dart' as _i4;
import '../resources/food_database_provider.dart' as _i5;
import 'dart:async' as _i6;
import '../resources/food_cache_provider.dart' as _i7;
import '../resources/repository.dart' as _i8;
import '../app.dart' as _i9;
import '../blocs/food_list_bloc.dart' as _i10;
import '../blocs/food_search_bloc.dart' as _i11;
import '../blocs/food_detail_bloc.dart' as _i12;

class BlocInjector$Injector implements _i1.BlocInjector {
  BlocInjector$Injector._(this._blocModule);

  final _i2.BlocModule _blocModule;

  _i3.Client _singletonClient;

  _i4.FoodApiProvider _singletonFoodApiProvider;

  _i5.FoodDatabaseProvider _singletonFoodDatabaseProvider;

  _i6.Future _singletonFuture;

  int _singletonInt;

  _i7.FoodCacheProvider _singletonFoodCacheProvider;

  _i8.Repository _singletonRepository;

  static _i6.Future<_i1.BlocInjector> create(_i2.BlocModule blocModule) async {
    final injector = BlocInjector$Injector._(blocModule);

    return injector;
  }

  _i9.App _createApp() => _i9.App(
      _createFoodListBloc(), _createFoodSearchBloc(), _createFoodDetailBloc());
  _i10.FoodListBloc _createFoodListBloc() =>
      _i10.FoodListBloc(_createRepository());
  _i8.Repository _createRepository() =>
      _singletonRepository ??= _blocModule.repository(_createFoodApiProvider(),
          _createFoodDatabaseProvider(), _createFoodCacheProvider());
  _i4.FoodApiProvider _createFoodApiProvider() => _singletonFoodApiProvider ??=
      _blocModule.foodApiProvider(_createClient());
  _i3.Client _createClient() => _singletonClient ??= _blocModule.client();
  _i5.FoodDatabaseProvider _createFoodDatabaseProvider() =>
      _singletonFoodDatabaseProvider ??= _blocModule.foodDatabaseProvider();
  _i7.FoodCacheProvider _createFoodCacheProvider() =>
      _singletonFoodCacheProvider ??=
          _blocModule.foodCacheProvider(_createFuture(), _createInt());
  _i6.Future _createFuture() =>
      _singletonFuture ??= _blocModule.sharedPreferences();
  int _createInt() => _singletonInt ??= _blocModule.minutesInCache();
  _i11.FoodSearchBloc _createFoodSearchBloc() =>
      _i11.FoodSearchBloc(_createRepository());
  _i12.FoodDetailBloc _createFoodDetailBloc() =>
      _i12.FoodDetailBloc(_createRepository());
  @override
  _i9.App get app => _createApp();
}
