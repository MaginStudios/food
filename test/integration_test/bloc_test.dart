import 'package:food/src/blocs/food_detail_bloc.dart';
import 'package:food/src/blocs/food_list_bloc.dart';
import 'package:food/src/blocs/food_search_bloc.dart';
import 'package:food/src/di/bloc_injector.dart';
import 'package:food/src/di/bloc_module.dart';
import 'package:food/src/models/food_detail_model.dart';
import 'package:food/src/models/food_search_model.dart';
import 'package:inject/inject.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  setUpAll(() {
    SharedPreferences.setMockInitialValues({});
  });

  group('BloC testing', () {
    /*TODO: Food List BLoC testing
       Problems testing with sqlflite:
       https://github.com/tekartik/sqflite/issues/83
       https://github.com/tekartik/sqflite/issues/49
    */
    test('Food Search BLoC testing', () async {
      final container = await BlocInjector.create(BlocModule());
      final foodSearchBloc = container.app.foodSearchBloc;
      foodSearchBloc.init();
      foodSearchBloc.fetchFoodSearch('Chicken');
      foodSearchBloc.foodSearch.listen(expectAsync1((value) {
        expect(value, isInstanceOf<FoodSearchModel>());
      }));
    });

    test('Food Detail BLoC testing', () async {
      final container = await BlocInjector.create(BlocModule());
      final foodDetailBloc = container.app.foodDetailBloc;
      foodDetailBloc.init();
      foodDetailBloc.fetchFoodDetailById(485365);
      foodDetailBloc.foodDetail.listen(expectAsync1((value) {
        expect(value, isInstanceOf<FoodDetailModel>());
      }));
    });
  });
}

class BlocTest {
  @provide
  BlocTest(
    this.foodListBloc,
    this.foodSearchBloc,
    this.foodDetailBloc,
  ) : super();

  final FoodListBloc foodListBloc;
  final FoodSearchBloc foodSearchBloc;
  final FoodDetailBloc foodDetailBloc;
}
