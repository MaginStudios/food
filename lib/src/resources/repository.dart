import 'package:food/src/models/food_list_model.dart';
import 'package:food/src/models/food_search_model.dart';
import 'package:food/src/models/state.dart';
import 'package:food/src/resources/food_api_provider.dart';
import 'package:food/src/resources/food_cache_provider.dart';
import 'package:food/src/resources/food_database_provider.dart';
import 'package:inject/inject.dart';

class Repository {
  @provide
  Repository(
      this.foodApiProvider, this.foodDatabaseProvider, this.foodCacheProvider);

  final FoodApiProvider foodApiProvider;
  final FoodDatabaseProvider foodDatabaseProvider;
  final FoodCacheProvider foodCacheProvider;

  Future<int> deleteFood(int id) => foodDatabaseProvider.delete(id);

  Future<int> insertFood(FoodListModel food) =>
      foodDatabaseProvider.insert(food);

  Future<int> updateFood(FoodListModel food) =>
      foodDatabaseProvider.update(food);

  Future<List<FoodListModel>> fetchFoodList(
          String orderByColumn, bool isDescending) =>
      foodDatabaseProvider.queryAllRows(orderByColumn, isDescending);

  Future<State> fetchFoodSearch(String keyword) async {
    final cacheState = await foodCacheProvider.fetchFoodSearch(keyword);
    if (cacheState is SuccessState) {
      return cacheState;
    }
    final resultState = await foodApiProvider.fetchFoodSearch(keyword);
    if (resultState is SuccessState) {
      foodCacheProvider.cacheFoodSearch(keyword, resultState.value);
    }
    return resultState;
  }

  Future<State> fetchFoodDetailById(int id) async {
    final cacheState = await foodCacheProvider.fetchFoodDetail([id]);
    if (cacheState is SuccessState) {
      return cacheState;
    }
    final resultState = await foodApiProvider.fetchFoodDetail([id]);
    if (resultState is SuccessState) {
      foodCacheProvider.cacheFoodDetail([id], resultState.value);
    }
    return resultState;
  }

  Future<State> fetchFoodDetailByIds(List<int> ids) async {
    final cacheState = await foodCacheProvider.fetchFoodDetail(ids);
    if (cacheState is SuccessState) {
      return cacheState;
    }
    final resultState = await foodApiProvider.fetchFoodDetail(ids);
    if (resultState is SuccessState) {
      foodCacheProvider.cacheFoodDetail(ids, resultState.value);
    }
    return resultState;
  }
}
